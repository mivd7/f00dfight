export interface IParticipant {
  id: number;
  name: string;
  number: number;
  votes: number;
  title: string;
  image_thumb?: string;
  image?: string;
  image_org?: string;
  image_fs_org?: string;
  image_fs?: string;
  image_fs_thumb?: string;
}

export interface IUser {
  firstname: string;
  lastname: string;
  shortname: string;
  year: string;
  month: string;
  day: string;
}
