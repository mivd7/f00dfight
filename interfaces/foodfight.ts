import { IParticipant } from './user';

export default interface IFoodFight {
  id: number;
  participants: IParticipant[];
  date: string;
  owner: string;
  subject: string;
  see_results?: boolean;
  image_thumb?: string;
  image?: string;
  image_org?: string;
  image_fs_org?: string;
  image_fs?: string;
  image_fs_thumb?: string;
}

export interface IVote {
  voter: string;
  first_place: string;
  second_place: string;
  third_place: string;
}

export interface ISelectOption {
  label: string;
  value: string;
}
