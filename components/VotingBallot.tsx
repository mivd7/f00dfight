import Select from 'react-select';
import { useApiData } from 'react-api-data';
import { useEffect, useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import StyledForm from './Form.style';
import { IParticipant, IUser } from '../interfaces/user';
import Image from 'next/image';
import { Grid } from '../pages/home.style';

interface VotingBallotProps {
  dishes: IParticipant[];
  foodFightId: number;
  earlyVotes: object[];
  onDone: () => void;
}

const VotingBallot: React.FC<VotingBallotProps> = ({ dishes, foodFightId, earlyVotes, onDone }) => {
  const { Heading1, Form, Label, Button } = StyledForm;
  const [inputValues, setInputValues] = useState({});
  const [userOptions, setUserOptions] = useState([]);
  const users = useApiData<IUser[]>('getUsers');
  const postVote = useApiData('vote');
  const [voteOptions, setVoteOptions] = useState(
    dishes.map((dish) => {
      return {
        value: dish.name,
        label: dish.title
      };
    })
  );

  useEffect(() => {
    if (users && users.data && userOptions.length === 0) {
      const options = users.data.map((user) => {
        return {
          value: user.shortname,
          label: user.firstname
        };
      });
      setUserOptions(options);
    }
  }, [users, userOptions]);

  const handleSubmit = (e) => {
    e.preventDefault();
    postVote.perform({ id: foodFightId }, inputValues);
    onDone();
  };

  const setVoter = (option) => {
    setInputValues({
      ...inputValues,
      voter: option.value
    });
  };

  const vote = (dish, place) => {
    setVoteOptions(voteOptions.filter((option) => option.value !== dish.value));
    let newInputValues = { ...inputValues };
    newInputValues[place] = dish.value;
    setInputValues(newInputValues);
  };

  return (
    <>
      <Heading1>Vote</Heading1>
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Label>
          I am
          {userOptions.length > 0 && <Select options={userOptions} name="colors" classNamePrefix="select" onChange={setVoter} />}
        </Label>
        <Grid gridTemplateColumns="repeat(3, 1fr)" style={{ width: '100%' }} noGap>
          <div></div>
          <div style={{ border: '1px solid black', padding: 10, borderBottom: 0, textAlign: 'center' }}>
            <Image priority src="/images/first-place.svg" layout="fixed" height={35} width={35} />
            {voteOptions && voteOptions.length > 0 && (
              <Select defaultValue={earlyVotes[0]} options={voteOptions} name="colors" classNamePrefix="select" onChange={(dish) => vote(dish, 'first_place')} />
            )}
          </div>
          <div></div>
        </Grid>
        <Grid gridTemplateColumns="repeat(3, 1fr)" style={{ width: '100%' }} noGap>
          <div style={{ border: '1px solid black', borderRight: 0, padding: 10, textAlign: 'center' }}>
            <Image priority src="/images/second-place.svg" layout="fixed" height={35} width={35} />
            {voteOptions && voteOptions.length > 0 && (
              <Select defaultValue={earlyVotes[1]} options={voteOptions} name="colors" classNamePrefix="select" onChange={(dish) => vote(dish, 'second_place')} />
            )}
          </div>
          <div style={{ borderBottom: '1px solid black', minHeight: '100%' }}></div>
          <div style={{ border: '1px solid black', borderLeft: 0, padding: 10, textAlign: 'center' }}>
            <Image priority src="/images/third-place.svg" layout="fixed" height={35} width={35} />
            {voteOptions && voteOptions.length > 0 && (
              <Select defaultValue={earlyVotes[2]} options={voteOptions} name="colors" classNamePrefix="select" onChange={(dish) => vote(dish, 'third_place')} />
            )}
          </div>
        </Grid>

        <Button type="submit">Vote</Button>
      </Form>
    </>
  );
};

export default VotingBallot;
