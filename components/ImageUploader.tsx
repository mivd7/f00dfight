import Image from 'next/image';
import ImageUploading from 'react-images-uploading';
import StyledForm from './Form.style';

interface ImageUploaderProps {
  images: any[];
  onChange: ([]) => void;
  existingImgUrl?: string;
}

const ImageUploader: React.FC<ImageUploaderProps> = ({ images, onChange, existingImgUrl }) => {
  const { ImageDropBox, ImageUploadButton, ImageUploadLabel } = StyledForm;
  return (
    <div className="App">
      <ImageUploading value={images} onChange={onChange} dataURLKey="data_url">
        {({ imageList, onImageUpload, onImageRemoveAll, onImageUpdate, onImageRemove, isDragging, dragProps }) => (
          // write your building UI
          <div className="upload__image-wrapper">
            {!images[0] && !existingImgUrl && (
              <ImageDropBox
                imageUploaded={false}
                onClick={(e) => {
                  e.preventDefault();
                  onImageUpload();
                }}
                {...dragProps}
              >
                <ImageUploadLabel>
                  <strong>Choose a file</strong>
                  <span className="box__dragndrop"> or drag it here</span>
                </ImageUploadLabel>
                <ImageUploadButton imageUploaded={false}>Upload</ImageUploadButton>
              </ImageDropBox>
            )}
            {!images[0] && existingImgUrl && (
              <ImageDropBox
                imageUploaded={false}
                onClick={(e) => {
                  e.preventDefault();
                  onImageUpload();
                }}
                {...dragProps}
              >
                <Image src={existingImgUrl} alt="uploaded image" width={200} height={200} layout="fixed" />
                <ImageUploadButton imageUploaded={false}>Upload</ImageUploadButton>
              </ImageDropBox>
            )}
            {images[0] && (
              <ImageDropBox imageUploaded={true}>
                <Image src={images[0]['data_url']} alt="uploaded image" width={200} height={200} layout="fixed" />
                <ImageUploadButton
                  onClick={(e) => {
                    e.preventDefault();
                    onImageRemove(0);
                  }}
                  imageUploaded={true}
                >
                  Remove
                </ImageUploadButton>
              </ImageDropBox>
            )}
          </div>
        )}
      </ImageUploading>
    </div>
  );
};

export default ImageUploader;
