import styled from 'styled-components';

interface ModalProps {
  handleClose: () => void;
  open: boolean;
  isTransparent?: boolean;
}

const ModalWrapper = styled.div`
  position: fixed;
  z-index: 1;
  padding-top: 50px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  max-width: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.4);
  max-height: 100%;
`;

const ModalBody = styled.div`
  background-color: ${(props) => (props.isTransparent ? 'transparent' : '#fefefe')};
  margin: auto;
  padding: 20px;
  border: ${(props) => (props.isTransparent ? 'none' : '1px solid #888')};
  width: 30%;
`;

const CloseButton = styled.span`
  color: ${(props) => (props.isTransparent ? '#fefefe' : '#aaaaaa')};
  float: right;
  font-size: 28px;
  font-weight: bold;

  &:hover,
  &:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
`;

const Modal: React.FC<ModalProps> = ({ children, handleClose, open, isTransparent }) => {
  return (
    <>
      {open && (
        <ModalWrapper onClick={handleClose}>
          <ModalBody isTransparent={isTransparent} onClick={(event) => event.stopPropagation()}>
            {!isTransparent && <CloseButton onClick={handleClose}>&times;</CloseButton>}
            {children}
          </ModalBody>
        </ModalWrapper>
      )}
    </>
  );
};

export default Modal;
