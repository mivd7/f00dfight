import Head from 'next/head';
import { Container } from '../pages/home.style';
import AppHeader from './AppHeader';

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>f00dfight</title>
        <meta name="referrer" content="no-referrer-when-downgrade" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AppHeader />
      <Container>
        <main>{children}</main>
      </Container>
    </>
  );
}
