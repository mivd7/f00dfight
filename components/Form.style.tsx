import styled from 'styled-components';

// Forms, inputs, buttons
const Form = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Input = styled.input`
  width: 100%;
  height: 35px;
  border: 1px solid #ccc;
  background-color: #fff;
  padding: 5px;
`;

const Button = styled.button`
  width: 100%;
  height: 35px;
  background-color: #5995ef;
  color: #fff;
  border-radius: 3px;
  margin-top: 5px;
`;

const ImageDropBox = styled.div`
  text-align: center;
  outline: 2px dashed #92b0b3;
  outline-offset: -10px;
  -webkit-transition: outline-offset 0.15s ease-in-out, background-color 0.15s linear;
  transition: outline-offset 0.15s ease-in-out, background-color 0.15s linear;
  font-size: 1.25rem;
  background-color: #c8dadf;
  position: relative;
  padding: ${(props) => (props.imageUploaded ? '20px' : '100px 20px')};
`;

const ImageUploadButton = styled.button`
  font-weight: 700;
  color: #e5edf1;
  background-color: #39bfd3;
  display: block;
  padding: 8px 16px;
  margin: ${(props) => (props.imageUploaded ? '5px auto 0' : '40px auto 0')};
`;

const ImageUploadLabel = styled.label`
  max-width: 80%;
  text-overflow: ellipsis;
  white-space: nowrap;
  cursor: pointer;
  display: inline-block;
  overflow: hidden;
}`;

const Heading1 = styled.h1`
  font-family: 'Raleway', sans-serif;
  font-weight: 600;
  color: #4d4d4d;
  font-size: 2.2em;
  margin: 5px 0;
`;

const Heading2 = styled.h2`
  font-family: 'Raleway', sans-serif;
  font-weight: 300;
  color: #4d4d4d;
  font-size: 1.8em;
`;

const Text = styled.p`
  font-family: 'Raleway', sans-serif;
  color: ${(props) => props.color || '#4d4d4d'};
`;

const Label = styled.label`
  display: flex;
  flex-direction: column;
  color: #777;
  width: 100%;
  font-family: 'Raleway', sans-serif;
  font-size: 0.8em;
  margin: 0.5em 0;
  position: relative;
`;

const StyledForm = {
  Form,
  Input,
  Button,
  Heading1,
  Heading2,
  Text,
  Label,
  ImageDropBox,
  ImageUploadButton,
  ImageUploadLabel
};

export default StyledForm;
