import { useState } from 'react';
import Link from 'next/link';
import { RestaurantMenu, Home } from '@styled-icons/material-outlined';
import styled from 'styled-components';
import Modal from '../components/Modal';
import FoodFightForm from '../components/FoodFightForm';
import { AddButton, BackButton, PageHeader, Title } from '../pages/home.style';
import { useRouter } from 'next/router';

const RestaurantIcon = styled(RestaurantMenu)`
  display: block;
  margin: auto;
  width: 35px;
  height: 35px;
  cursor: pointer;
`;

const HomeIcon = styled(Home)`
  display: block;
  margin: auto;
  width: 35px;
  height: 35px;
  cursor: pointer;
`;

export default function AppHeader() {
  const [formOpened, setFormOpened] = useState(false);
  const router = useRouter();
  const isHome = router.pathname === '/';

  return (
    <>
      {formOpened && (
        <Modal open={formOpened} handleClose={() => setFormOpened(false)}>
          <FoodFightForm onDone={() => setFormOpened(false)} />
        </Modal>
      )}
      <PageHeader>
        {!isHome && (
          <BackButton onClick={() => router.push('/')}>
            <HomeIcon />
            <div>Back to home</div>
          </BackButton>
        )}
        <Title>Welcome to f00dfight!</Title>
        <AddButton onClick={() => setFormOpened(true)}>
          <RestaurantIcon />
          <div>Add new</div>
        </AddButton>
      </PageHeader>
    </>
  );
}
