import { useState } from 'react';
import { useApiData } from 'react-api-data';
import { IParticipant } from '../interfaces/user';
import StyledForm from './Form.style';
import ImageUploader from './ImageUploader';

interface ParticipantFormProps {
  participant: IParticipant;
  onDone: () => void;
}
const ParticipantForm: React.FC<ParticipantFormProps> = ({ participant, onDone }) => {
  const { Form, Label, Input, Button } = StyledForm;
  const [title, setTitle] = useState(participant.title);
  const [images, setImages] = useState([]);
  const [newImage, setNewImage] = useState(null);
  const editParticipation = useApiData('editParticipation');

  const handleImageChange = (imageList) => {
    setImages(imageList);
    if (imageList[0] && imageList[0].file) {
      setNewImage(imageList[0].file);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('title', title);
    if (newImage) {
      formData.append('image', newImage);
    }
    editParticipation.perform({ id: participant.id }, formData);
    onDone();
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Label>
        Title
        {participant && <Input defaultValue={participant.title} type="text" name="title" placeholder="Dish name" onChange={(e) => setTitle(e.target.value)} />}
      </Label>
      <Label>
        Upload image
        <ImageUploader images={images} onChange={handleImageChange} existingImgUrl={participant.image} />
      </Label>
      <Button type="submit">Submit</Button>
    </Form>
  );
};

export default ParticipantForm;
