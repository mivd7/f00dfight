import Select from 'react-select';
import { useActions, useApiData } from 'react-api-data';
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import IFoodFight from '../interfaces/foodfight';
import { IParticipant, IUser } from '../interfaces/user';
import StyledForm from './Form.style';
import formStyles from './form.module.css';
import ImageUploader from './ImageUploader';

interface FoodFightFormProps {
  editMode?: boolean;
  editItem?: IFoodFight;
  onDone: () => void;
}

interface ParticipantInputProps {
  existingParticipants: IParticipant[];
  users: IUser[];
  selectParticipants: (selectedParticipants: IParticipant) => void;
}

const ParticipantInput: React.FC<ParticipantInputProps> = ({ existingParticipants, users, selectParticipants }) => {
  const options = users.map((user) => {
    return {
      value: user.shortname,
      label: user.firstname
    };
  });

  return (
    <>
      {!existingParticipants && <Select isMulti name="colors" options={options} className="basic-multi-select" classNamePrefix="select" onChange={selectParticipants} />}
      {existingParticipants && (
        <Select
          defaultValue={existingParticipants.map((participant) => {
            return {
              value: participant.name,
              label: participant.name
            };
          })}
          isMulti
          name="colors"
          options={options}
          className="basic-multi-select"
          classNamePrefix="select"
          onChange={selectParticipants}
        />
      )}
    </>
  );
};

const FoodFightForm: React.FC<FoodFightFormProps> = ({ editMode, editItem, onDone }) => {
  const { Heading1, Form, Label, Input, Button } = StyledForm;
  const [inputValues, setInputValues] = useState<IFoodFight>({} as IFoodFight);
  const [date, setDate] = useState(new Date());
  const [images, setImages] = useState([]);

  const users = useApiData<IUser[]>('getUsers');
  const actions = useActions();

  useEffect(() => {
    if (editMode) {
      setInputValues(editItem);
    }
  }, [editMode]);

  useEffect(() => {
    if (!inputValues.date && !editMode) {
      //set default value of date to today if none selected
      setInputValues({
        ...inputValues,
        date: moment(date).format('YYYY-MM-DD')
      });
    }
  }, [date, inputValues]);

  const handleInputChange = (field: string, inputValue: string) => {
    let newInputValues = { ...inputValues };
    newInputValues[field] = inputValue;
    setInputValues(newInputValues);
  };

  const handleImageChange = (imageList) => {
    setImages(imageList);
    if (imageList[0] && imageList[0].file) {
      setInputValues({
        ...inputValues,
        image: imageList[0].file
      });
    }
  };

  const selectParticipants = (selectedParticipants) => {
    //API requires comma seperated string of selected participants
    const participants = selectedParticipants.map((selected) => selected.value).join(', ');
    setInputValues({
      ...inputValues,
      participants
    });
  };

  const handleDateChange = (date) => {
    setDate(date);
    setInputValues({
      ...inputValues,
      date: moment(date).format('YYYY-MM-DD')
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    //convert input values obj into formdata format to post to API
    Object.keys(inputValues).map((key) => formData.append(key, inputValues[key]));

    if (!editMode) {
      actions.perform('createFoodFight', {}, formData);
    } else {
      actions.perform('editFoodFight', { id: editItem.id }, formData);
    }
    actions.invalidateCache('getFoodFights');
    onDone();
  };

  return (
    <>
      <Heading1>{!editMode ? 'Start a ' : 'Edit '}Foodfight</Heading1>
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Label>
          Subject
          <Input defaultValue={editItem ? editItem.subject : ''} type="text" name="subject" placeholder="Subject" onChange={(e) => handleInputChange('subject', e.target.value)} />
        </Label>
        <Label>
          Owner
          <Input defaultValue={editItem ? editItem.owner : ''} type="text" name="owner" placeholder="Owner" onChange={(e) => handleInputChange('owner', e.target.value)} />
        </Label>
        <Label>
          Date
          <DatePicker className={formStyles.datePicker} selected={date} onChange={(date) => handleDateChange(date)} />
        </Label>
        <Label>
          Participants
          {users && users.data && users.data.length > 0 && (
            <ParticipantInput existingParticipants={editMode ? editItem.participants : null} users={users.data} selectParticipants={selectParticipants} />
          )}
        </Label>
        <Label>
          Upload image
          {!editMode && <ImageUploader images={images} onChange={handleImageChange} />}
          {editMode && <ImageUploader images={images} onChange={handleImageChange} existingImgUrl={editItem.image} />}
        </Label>
        <Button type="submit">Submit</Button>
      </Form>
    </>
  );
};

export default FoodFightForm;
