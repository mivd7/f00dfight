import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { configure, reducer } from 'react-api-data';
import thunk from 'redux-thunk';

const endpointConfig: any = {
  getFoodFights: {
    url: 'http://localhost:3000/api/',
    method: 'GET'
  },
  getUsers: {
    url: 'http://localhost:3000/api/users?_format=json&mode=2',
    method: 'GET'
  },
  createFoodFight: {
    url: 'http://localhost:3000/api/create?_format=json',
    method: 'POST'
  },
  deleteFoodFight: {
    url: 'http://localhost:3000/api/delete/:id',
    method: 'POST'
  },
  editFoodFight: {
    url: 'http://localhost:3000/api/edit/:id',
    method: 'POST'
  },
  vote: {
    url: 'http://localhost:3000/api/vote/:id',
    method: 'POST'
  },
  editParticipation: {
    url: 'http://localhost:3000/api/participant/:id',
    method: 'POST'
  }
};

const reducers = combineReducers({ apiData: reducer });
const enhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools(applyMiddleware(thunk)) : applyMiddleware(thunk);

const store = createStore(reducers, enhancer);
store.dispatch(configure({}, endpointConfig));

export default store;
