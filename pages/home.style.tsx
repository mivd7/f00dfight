import styled from 'styled-components';

export const Container = styled.section`
  padding: 0 1rem;
  margin: 1rem auto 2rem;
  overflow: auto;
`;

export const Title = styled.h1`
  font-size: 36px;
  color: ${({ theme }) => theme.colors.primary};
`;

export const AddButton = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  background-color: ${({ theme }) => theme.colors.primary};
  border: 1px solid black;
  border-radius: 10px;
  padding: 5px;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.textColor};
  align-items: center;
`;

export const BackButton = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
  background-color: ${({ theme }) => theme.colors.primary};
  border: 1px solid black;
  border-radius: 10px;
  padding: 5px;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.textColor};
  align-items: center;
`;

export const Grid = styled.div`
  padding: 10px;
  align-items: center;
  align-self: center;
  display: grid;
  grid-template-columns: ${(props) => props.gridTemplateColumns};
  grid-auto-rows: auto;
  grid-gap: ${(props) => (props.noGap ? '0' : '1rem')};
  @media only screen and (max-width: 1500px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media only screen and (max-width: 1000px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media only screen and (max-width: 500px) {
    grid-template-columns: 1fr;
  }
`;

export const ListItem = styled.div`
  text-align: ${(props) => (props.align ? props.align : 'center')};
  background-color: white;
  height: 100%;
  justify-content: center;
  align-items: center;
  border: 2px solid #e7e7e7;
  border-radius: 4px;
  padding: 0.5rem;
`;

export const Plate = styled.li`
  display: flex;
  flex-wrap: wrap;
  height: fit-content;
  width: fit-content;
  align-items: center;
  background-color: #e4e4e4;
  border-radius: 50%;
  justify-content: center;
  font-size: 15px;
  box-shadow: 2px 2px 8px 2px #000;
  padding: 30px;
  margin: 20px;
  transition-duration: 0.25s;
  &:hover {
    transform: scale(1.07);
  }
`;
// flex-direction: column;
//   height: fit-content;
//   width: fit-content;
export const InnerPlate = styled.span`
  align-self: center;
  border-radius: 50%;
  height: fit-content;
  width: fit-content;
  border: #dddddd solid 2px;
  background-color: #d2d2d2;
  font-size: 1rem;
  box-shadow: 0 0 10px 10px #cfcfcf;
  justify-content: center;
  padding: 25px;
  text-align: center;

  img {
    object-fit: cover;
    margin-top: 5px;
    min-width: 100px;
    border-radius: 100%;
    margin: 25px 2px;
    align-self: center;
  }
`;

export const FoodFightLabel = styled.div`
  width: 90%;
  background-color: ${({ theme }) => theme.colors.secondary};
  border: 1px solid black;
  padding: 10px;
  h2 {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 32px;
    cursor: pointer;
  }
  &:hover {
    box-shadow: 1px 1px 4px 1px #000;
  }
`;
export const Text = styled.div`
  font-family: 'Raleway', sans-serif;
  color: ${(props) => (props.color ? props.color : props.theme.colors.textColor)};
`;

export const PageHeader = styled.header`
  position: relative;
  padding: 25px;
  background-color: ${({ theme }) => theme.colors.secondary};
  display: flex;
  flex-direction: column;
  align-items: center;
`;
