import Image from 'next/image';
import { useRouter } from 'next/router';
import { useApiData } from 'react-api-data';

import IFoodFight from '../interfaces/foodfight';
import Layout from '../components/Layout';
import { Grid, Plate, InnerPlate, Text, FoodFightLabel } from './home.style';

export default function Index({ theme }) {
  const foodFightsEndpoint = useApiData<IFoodFight[]>('getFoodFights');
  const foodFights = foodFightsEndpoint.data;
  const router = useRouter();

  return (
    <Layout>
      <Grid gridTemplateColumns={'repeat(4, 1fr)'}>
        {foodFights &&
          foodFights.map((fight) => (
            <ul key={fight.id}>
              <Plate onClick={() => router.push(`/foodfights/${fight.id}`)}>
                <InnerPlate>
                  {fight.image_fs && <Image priority src={fight.image_thumb} layout="fixed" height={300} width={300} />}
                  {!fight.image_fs && <Image priority src="/images/food-img-placeholder.jpg" layout="fixed" height={300} width={300} />}
                </InnerPlate>
              </Plate>

              <FoodFightLabel onClick={() => router.push(`/foodfights/${fight.id}`)}>
                <h2>{fight.subject}</h2>
                <Text color={theme.colors.primary}>
                  <strong>Participants: </strong>
                </Text>
                {fight.participants &&
                  fight.participants.map((participant, index) => (
                    <span style={{ color: 'white' }} key={participant.id}>
                      {participant.name}

                      {index !== fight.participants.length - 1 ? ', ' : ''}
                    </span>
                  ))}
                <Text color={theme.colors.primary}>
                  <strong>Started: </strong>
                  <br />
                  <span style={{ color: 'white' }}>{fight.date}</span>
                </Text>
              </FoodFightLabel>
            </ul>
          ))}
      </Grid>
    </Layout>
  );
}
