import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import Head from 'next/head';
import styled from 'styled-components';
import { HandThumbsUp, PencilSquare, Trash } from '@styled-icons/bootstrap';
import { HowToVote } from '@styled-icons/material-outlined';

import Layout from '../../components/Layout';
import { useRouter } from 'next/dist/client/router';
import { useActions, useApiData } from 'react-api-data';
import Form from '../../components/FoodFightForm';
import Modal from '../../components/Modal';
import { Grid, Title, Text, ListItem } from '../home.style';
import VotingBallot from '../../components/VotingBallot';
import ParticipantForm from '../../components/ParticipantForm';
import IFoodFight, { ISelectOption } from '../../interfaces/foodfight';
import { IParticipant } from '../../interfaces/user';

const ThumbIcon = styled(HandThumbsUp)`
  width: 35px;
  height: 35px;
  cursor: pointer;
  fill: ${(props) => (props.selected ? 'green' : 'black')};
`;

const EditIcon = styled(PencilSquare)`
  width: 35px;
  height: 35px;
  margin: 2.5px;
  cursor: pointer;
`;

const DeleteIcon = styled(Trash)`
  width: 35px;
  height: 35px;
  margin: 2.5px;
  cursor: pointer;
`;

const VoteIcon = styled(HowToVote)`
  width: 35px;
  height: 35px;
  margin: 2.5px;
  cursor: pointer;
`;

export default function FoodFight() {
  const router = useRouter();
  const { id } = router.query;
  const foodFightsEndpoint = useApiData<IFoodFight[]>('getFoodFights');
  const foodFights = foodFightsEndpoint.data;
  const actions = useActions();

  const [foodFight, setFoodFight] = useState<IFoodFight>(null);
  const [earlyVotes, setEarlyVotes] = useState<ISelectOption[]>([]);
  const [editParticipant, setEditParticipant] = useState({} as IParticipant);
  const [showEditForm, setShowEditForm] = useState(false);
  const [showEditParticipation, setShowEditParticipation] = useState(false);
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [showBallot, setShowBallot] = useState(false);
  const [enlargedImage, setEnlargedImage] = useState(null);

  const findFoodFightById = (id) => {
    const foundFoodFight = foodFights.find((foodFight) => foodFight.id === Number(id));
    setFoodFight(foundFoodFight);
  };

  useEffect(() => {
    if (earlyVotes.length === 3) {
      //when a user has given three dishes a thumb up show the voting ballot;
      setShowBallot(true);
    }
  }, [earlyVotes]);

  useEffect(() => {
    if (foodFights) {
      findFoodFightById(id);
    }
  }, [foodFights]);

  const handleDelete = () => {
    const deleteId = foodFight.id;
    actions.perform('deleteFoodFight', { id: deleteId });
    setShowDeleteDialog(false);
    actions.invalidateCache('getFoodFights');
    router.push('/');
  };

  const handleThumbsUpClick = (vote: ISelectOption) => {
    const { value, label } = vote;
    if (earlyVotes.length < 3 && earlyVotes.findIndex((earlyVote) => earlyVote.value === vote.value) === -1) {
      setEarlyVotes([...earlyVotes, { value, label }]);
    } else {
      setEarlyVotes(earlyVotes.filter((earlyVote) => earlyVote.value !== vote.value));
    }
  };

  return (
    <Layout>
      {showEditForm && (
        <Modal open={showEditForm} handleClose={() => setShowEditForm(false)}>
          <Form editMode={true} editItem={foodFight} onDone={() => setShowEditForm(false)} />
        </Modal>
      )}
      {showEditParticipation && (
        <Modal open={showEditParticipation} handleClose={() => setShowEditParticipation(false)}>
          <ParticipantForm participant={editParticipant} onDone={() => setShowEditParticipation(false)} />
        </Modal>
      )}
      {showDeleteDialog && (
        <Modal open={showDeleteDialog} handleClose={() => setShowDeleteDialog(false)}>
          <Text>Are you sure you want to delete this foodfight?</Text>
          <button onClick={handleDelete}>Yes</button>
          <button onClick={() => setShowDeleteDialog(false)}>No</button>
        </Modal>
      )}
      {showBallot && (
        <Modal open={showBallot} handleClose={() => setShowBallot(false)}>
          {foodFights && foodFight && <VotingBallot dishes={foodFight.participants} earlyVotes={earlyVotes} onDone={() => setShowBallot(false)} foodFightId={foodFight.id} />}
        </Modal>
      )}
      {enlargedImage && (
        <Modal isTransparent open={enlargedImage} handleClose={() => setEnlargedImage(null)}>
          <Image priority src={enlargedImage.src} layout="intrinsic" height={1200} width={1200} />
          <Text>
            <strong>{enlargedImage.description}</strong>
          </Text>
        </Modal>
      )}
      {foodFight && (
        <>
          <Head>
            <title>{foodFight.subject}</title>
          </Head>
          <Grid gridTemplateColumns=".5fr 2fr">
            <div>
              {foodFight.image_fs && <Image priority src={foodFight.image_fs || '/images/food-img-placeholder.jpg'} layout="intrinsic" height={400} width={400} />}
              {!foodFight.image_fs && <Image priority src="/images/food-img-placeholder.jpg" layout="intrinsic" height={400} width={400} />}
              <Text>
                <Title style={{ textAlign: 'left' }}>{foodFight.subject}</Title>
                <strong>Started by: </strong>
                <div>{foodFight.owner}</div>
                <br />
                <strong>Participants: </strong>
                {foodFight.participants && foodFight.participants.map((participant) => <Text key={participant.id}>{participant.name}</Text>)}
                <br />
                <div>
                  <strong>Started: </strong>
                  <div>{foodFight.date}</div>
                </div>
                <br />
                <strong>Actions: </strong>
                <div>
                  <DeleteIcon onClick={() => setShowDeleteDialog(true)} />
                  <EditIcon onClick={() => setShowEditForm(true)} />
                </div>
                <br />
                <strong>Vote: </strong>
                <div>
                  <VoteIcon onClick={() => setShowBallot(true)} />
                </div>
              </Text>
            </div>
            <div>
              <Title style={{ textAlign: 'center' }}>Foodfight Leaderboard</Title>
              {foodFight.participants &&
                foodFight.participants
                  .sort((a, b) => b.votes - a.votes)
                  .map((participant, index) => (
                    <ListItem key={participant.id} align="left">
                      <Grid gridTemplateColumns="1fr 1fr 1fr">
                        <div>
                          <Title>{index + 1}</Title>
                          {participant.image_thumb && (
                            <Image
                              priority
                              src={participant.image_thumb}
                              onClick={() =>
                                setEnlargedImage({
                                  src: participant.image_org,
                                  description: participant.title
                                })
                              }
                              layout="fixed"
                              height={125}
                              width={125}
                            />
                          )}
                        </div>
                        <div>
                          <Text color="black">Name: {participant.name}</Text>
                          <Text color="black">Votes: {participant.votes}</Text>
                          <Text color="black">Dish: {participant.title}</Text>
                        </div>

                        <div>
                          <ThumbIcon
                            onClick={() => handleThumbsUpClick({ value: participant.name, label: participant.title })}
                            selected={earlyVotes.findIndex((vote) => vote.value === participant.name) !== -1}
                          />
                          <EditIcon
                            onClick={() => {
                              setEditParticipant(participant);
                              setShowEditParticipation(true);
                            }}
                          />
                          <DeleteIcon />
                        </div>
                      </Grid>
                    </ListItem>
                  ))}
              <br />
            </div>
          </Grid>
        </>
      )}
    </Layout>
  );
}
