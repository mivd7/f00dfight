import { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import '../styles/global.css';
import store from '../store';

const theme = {
  colors: {
    primary: '#a4e470',
    secondary: '#3C891A',
    textColor: '#efefef'
  }
};

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} theme={theme} />
      </ThemeProvider>
    </Provider>
  );
}
